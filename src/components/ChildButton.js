import styled from "styled-components";
import SuperButton from "./superButton";

const ChildButton = styled(SuperButton)`
    background-color: ${props => props.bg};
`;

export default ChildButton;
