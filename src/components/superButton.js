import styled from "styled-components";

const SuperButton = styled.button`
    padding: 10px;
    border: none;
    border-radius: 5px;
    font-size: 1.25rem;
`

export default SuperButton;
